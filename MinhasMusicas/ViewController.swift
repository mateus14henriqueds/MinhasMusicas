//
//  ViewController.swift
//  MinhasMusicas
//
//  Created by COTEMIG on 18/08/22.
//

import UIKit

struct Musica {
    let nomeMusica : String
    let nomeAlbum : String
    let nomeCantor : String
    let nomeImagemPequena : String
    let nomeimagemGrande : String

}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var listaDeMusica:[Musica] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listaDeMusica.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let musica = self.listaDeMusica[indexPath.row]
        
        cell.musica.text = musica.nomeMusica
        cell.album.text = musica.nomeAlbum
        cell.cantor.text = musica.nomeCantor
        cell.capa.image = UIImage(named: musica.nomeImagemPequena)
        return cell
    }
    
    
    @IBOutlet weak var tableview: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableview.dataSource = self
        self.tableview.delegate = self
        
        self.listaDeMusica.append(Musica(nomeMusica: "Pontos Cardeais", nomeAlbum: "Álbum Vivo!", nomeCantor: "Alceu Valença", nomeImagemPequena: "capa_alceu_pequeno", nomeimagemGrande: "capa_alceu_grande"))
        
        self.listaDeMusica.append(Musica(nomeMusica: "Menor Abandonado", nomeAlbum: "Álbum Patota de Cosme", nomeCantor: "Zeca Pagodinho", nomeImagemPequena: "capa_zeca_pequeno", nomeimagemGrande: "capa_zeca_grande"))
        
        self.listaDeMusica.append(Musica(nomeMusica: "Tiro do Álvaro", nomeAlbum: "Álbum Adoniram Barbosa e Convidados!", nomeCantor: "Adoniram Barbosa", nomeImagemPequena: "capa_adoniran_pequeno", nomeimagemGrande: "capa_adhoniran_grande"))
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "abrirDetalhe", sender: indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detalheViewController = segue.destination as! DetalheMusicaViewController
        let indice = sender as! Int
        let musica = self.listaDeMusica[indice]
        detalheViewController.nomeImagem = musica.nomeimagemGrande
        detalheViewController.nomeMusica = musica.nomeMusica
        detalheViewController.nomeAlbum = musica.nomeAlbum
        detalheViewController.nomeCantor = musica.nomeCantor
    }
    
}

